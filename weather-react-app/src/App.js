import logo from './logo.svg';
import './App.css';
import React from 'react';
import WeeklyWeather from './components/WeeklyWeather';
import Dailyweather from './components/DailyWeather';
import Recordweather from './components/RecordWeather';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,  
  useLocation
} from "react-router-dom";
import moment from 'moment'

function App() {
  return (

<Router>
    <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/weather">WeeklyWeather</Link>
            </li>
            <li>
              <Link to="/forecastforday">DailyWeather</Link>
            </li>
            <li>
              <Link to="/records">RecordWeather</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
      <div className="App">
         
        <section> 
          <Switch>
            <Route path="/weather">
              <WeeklyWeather />
            </Route>
            <Route path="/forecastforday">
              <DailyWeather />
            </Route>
            <Route path="/records">
              <Recordweather />
            </Route>
            <Route path="/">
              <h1>Home</h1>
              Welcome to our service. Please explore <Link to="/weather">WeeklyWeather</Link> per week, or <Link to="/forecastforday">DailyWeather</Link>
            </Route>

            <Route path="*">
              <NoMatch />
            </Route>
            
          </Switch>
          

        </section>
      </div>
    </div>
    </Router>
  );
}

function RecordWeather() {
    let location = useLocation();
  
    return (
      <div>
        <h2>
        RecordsWeather
        </h2>
        <ul>
          <li>img: http://placeimg.com/640/480/city</li>
          <li>address: Port Taryn United States of America</li>
          <li>date: Wed Apr 29 2020 22:48:00 GMT+0300 (Moscow Standard Time)</li>
          <li>description: Labore ea illum accusamus. Placeat magnam aperiam atque qui. Quidem qui ducimus</li>
        </ul>
      </div>
    );
  
  }

function DailyWeather() {
let location = useLocation();

return (
<div>
<h2>
DailyWeather
</h2>
<ul>
<li>img: http://placeimg.com/640/480/city</li>
<li>place: United Kingdom</li>
<li>date: Friday</li>
<li>degree: 33`</li>
</ul>
</div>
);
}


function NoMatch() {
  let location = useLocation();

  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}
export default App;
