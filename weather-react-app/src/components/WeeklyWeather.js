import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import moment from 'moment'
import Moment from 'react-moment';

const api = new Api.DefaultApi()

class WeeklyWeather extends React.Component {

    constructor(props) {
        super(props);
        this.state = { weather: [] };

        this.handleReload = this.handleReload.bind(this);
    }


    async handleReload(event) {
        const response = await api.weather({ date: '' });
        this.setState({ weather: response });
        console.log(this.state.weather);
    }


    render() {
        return <div>
            <h1>Weekly weather forecast</h1>
            <h3>Weather starting from <Moment format="YYYY/MM/DD">{this.state.date}</Moment> </h3>
            <ul>
            {this.state.weather.map(
                   (event) => 
                        <li key={event.degree}>{event.weekday} {event.img}:  {event.degree} is performing in {event.place}</li>)}
            </ul>
            <button onClick={this.handleReload}>Reload</button>
        </div>
    }
}

export default WeeklyWeather;