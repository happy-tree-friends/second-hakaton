import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import moment from 'moment'
import Moment from 'react-moment';

const api = new Api.DefaultApi()

class RecordWeather extends React.Component {

    constructor(props) {
        super(props);
        this.state = { items: [] };

        this.handleReload = this.handleReload.bind(this);
        // this.handleReload();
    }


    async handleReload(event) {
        const response = await api.records({ date: '' });
        this.setState({ items: response });
        console.log(this.state.items);
    }


    render() {
        return <div>
        <h1>Today's weather record</h1>
          
          <ul>
            {this.state.items.map(
                   (event) => 
                   <li key={event.temperature}>{event.temperature} {event.address}:  {event.img} is performing in {event.date} </li>)
                    }
            </ul>

            <button onClick={this.handleReload}>Reload</button>
        </div>
    }
}


export default RecordWeather;