import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import moment from 'moment'
import Moment from 'react-moment';


const api = new Api.DefaultApi()

class DailyWeather extends React.Component {

    constructor(props) {
        super(props);
         this.state = { forecastforday: [{
img: "http://placeimg.com/640/480/city", place: "United Kingdom", date: "Friday", degree: "83700" }] 
};

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


    async handleReload(event) {
        const response = await api.forecastforday({ date: '10-10-2020' });
        this.setState({ forecastforday: response });
    }


    render() {
        return <div>
            <h2>Daily Weather</h2>
          
            
         <ul>
            {this.state.forecastforday.map(
                   (event) => 
                        <li key={event.degree}>{event.date} {event.img}:  {event.degree} is performing in {event.place}</li>)}
           </ul> 

            <button onClick={this.handleReload}>Reload</button>
        </div>
    }
}

export default withRouter(DailyWeather);