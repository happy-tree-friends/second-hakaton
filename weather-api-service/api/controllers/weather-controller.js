'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*
 Modules make it possible to import JavaScript files into your application.  Modules are imported
 using 'require' statements that give you a reference to the module.

  It is a good idea to list the modules that your application depends on in the package.json in the project root
 */
var util = require('util');
const jsf = require('json-schema-faker');
const chance = require('chance');
jsf.extend('chance', () => new chance.Chance());
var faker = require('faker');

/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

 For a controller in a127 (which this is) you should export the functions referenced in your Swagger document by name.
            
 Either:
  - The HTTP Verb of the corresponding operation (get, put, post, delete, etc)
  - Or the operationId associated with the operation in your Swagger document

  In the starter/skeleton project the 'get' operation on the '/hello' path has an operationId named 'hello'.  Here,
  we specify that in the exports of this module that 'hello' maps to the function named 'hello'
 */
module.exports = {
  weather: getWeather,
  records: getRecords,
  forecastforday: GetForecastForDay
};


var schema = {
  "type": "array",
  "minItems": 7,
  "maxItems": 7,
  "items": {
    "type": "object",
    "required": [
      "weekday", "img", "degree", "place"
    ],
    "properties": {
      "weekday": {
        "type": "string",
        "faker": "date.weekday"
      },
      "img": {
        "type": "string",
        "faker": "image.nature"
      },
      "degree": {
        "type": "integer",
        "faker": "random.number"
      },
      "place": {
        "type": "string",
        "faker": "address.country"
      },
    }
  }
}


/*
  Functions in a127 controllers used for operations should take two parameters:
         
  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */

function getWeather(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  var date = req.swagger.params.date.value || '2020-04-20';
  var title = util.format('Weather, %s!', date);
  jsf.resolve(schema).then(sample => res.json(sample));
}

function getRecords(req, res) {
  var date = req.swagger.params.date.value || '2020-04-20';
  var title = util.format('Weather %s', date);
  res.json([
    {
      "temperature": faker.random.number(),
      "address": faker.address.city() + ' ' + faker.address.country(),
      "img": '' + faker.image.city(),
      "date": '' + faker.date.past(),
      "description": '' + faker.lorem.paragraph()
    }]);
}

function GetForecastForDay(req, res) {
  var date = req.swagger.params.date.value || '2020-04-20';
  var title = util.format('Weather %s', date);
  res.json([
    {
      "img": '' + faker.image.city(),
      "place": '' + faker.address.country(),
      "date": '' + faker.date.weekday(),
      "degree": faker.random.number()
    }]);
}

